YAWC - Yet Another Web Chat
===========================

Build
-----

```bash
docker build -t yawc -f docker/app/Dockerfile .
```

Run
---

Run in development mode (with mounted application code):
```bash
cd compose/yawcdev && docker-compose run app yawc migrate && docker-compose up
```
and open http://localhost:8000/ in the web browser.
