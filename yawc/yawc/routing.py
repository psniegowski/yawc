# from channels.routing import route
from chat.consumers import ChatConsumer

channel_routing = [
    ChatConsumer.as_route(),
]
