from django.shortcuts import render
import uuid


def main_page(request):

    return render(request, 'chat/chat.html', {
        'client_uuid': uuid.uuid4()
    })
