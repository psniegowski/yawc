from channels.generic.websockets import JsonWebsocketConsumer
from channels import Group
import logging


class ChatConsumer(JsonWebsocketConsumer):

    # Set to True to automatically port users from HTTP cookies
    # (you don't need channel_session_user, this implies it)
    http_user = True

    # Set to True if you want it, else leave it out
    strict_ordering = False

    groups = [ 'maingroup' ]

    def connect(self, message, **kwargs):
        """
        Perform things on connection start
        """
        # Accept the connection; this is done by default if you don't override
        # the connect function.
        # logging.error('connected: %s', message.reply_channel)

        self.message.reply_channel.send({"accept": True})
        self.send(content={ 'text': 'welcome to YAWC!' })


    def receive(self, content, **kwargs):
        """
        Called when a message is received with either text or bytes
        filled out.
        """
        self.group_send('maingroup', content=content)


    def disconnect(self, message, **kwargs):
        """
        Perform things on connection close
        """
        pass
