(function() {
  new Vue({
    el: '#app',
    data: {
      messages: [
      ],
      nextMessage: "",
    },
    methods: {
      send: function() {
        if (this.nextMessage !== "") {
          this.websocket.send(JSON.stringify({
            text: this.nextMessage,
            sender: this.clientUuid,
          }));
          this.nextMessage = "";
        }
      },
      connect: function() {
        this.websocket = new WebSocket(window.location.href.replace(/^http/, 'ws'));

        this.websocket.onopen = function () {
          console.log('open')
        }

        this.websocket.onmessage = function (evt) {
          var parsed = JSON.parse(evt.data);
          if (parsed.sender !== this.clientUuid) {
            this.messages.unshift(parsed.text);
          }
        }.bind(this);

        this.websocket.onclose = function () {
          console.log('closed')
          this.websocket = undefined;
          if (this.keepConnecting) {
            this.connect();
          }
        }.bind(this);
      }
    },
    created: function() {
      this.keepConnecting = true;
      this.clientUuid = window.clientUuid;
    },
    mounted: function() {
      this.connect();
    },
    beforeDestroy: function() {
      this.keepConnecting = false;
      if (this.websocket !== undefined) {
        this.websocket.close();
      }
    }
  })
}());
